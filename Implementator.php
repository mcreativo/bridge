<?php


interface Implementator
{

    public function doStep1();

    public function doStep2();

    public function doStep3();
} 