<?php


class ImplementatorA implements Implementator
{
    public function doStep1()
    {
        echo 'ImplementatorA doing step 1' . PHP_EOL;
    }

    public function doStep2()
    {
        echo 'ImplementatorA doing step 2' . PHP_EOL;
    }

    public function doStep3()
    {
        echo 'ImplementatorA doing step 3' . PHP_EOL;
    }
} 