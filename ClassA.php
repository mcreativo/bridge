<?php


class ClassA
{
    /**
     * @var Implementator
     */
    protected $implementator;

    /**
     * @param Implementator $implementator
     */
    function __construct(Implementator $implementator)
    {
        $this->implementator = $implementator;
    }

    public function doA()
    {
        $this->implementator->doStep1();
        $this->implementator->doStep2();
        $this->implementator->doStep3();
    }
}
