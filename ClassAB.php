<?php


class ClassAB extends ClassA
{
    public function doB()
    {
        $this->implementator->doStep2();
        $this->implementator->doStep3();
        $this->implementator->doStep1();
    }
} 