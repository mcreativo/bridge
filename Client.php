<?php

$implementatorA = new ImplementatorA();
$implementatorB = new ImplementatorB();


$classA = new ClassA($implementatorA);
$classA->doA();

$classAB = new ClassAB($implementatorB);
$classAB->doB();